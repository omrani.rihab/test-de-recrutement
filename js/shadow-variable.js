const articleList = []; // In a real app this list would be full of articles.
const MAX_KUDOS = 5;

function calculateTotalKudos(articles) {
const totalKudos = articles.reduce((acc, article) => acc + article.kudos, 0);
return totalKudos;
}

document.write(`
<p>Maximum kudos you can give to an article: ${kudos}</p>
<p>Total Kudos already given across all articles: ${calculateTotalKudos(articleList)}</p>
`);