<?php

declare(strict_types=1);

namespace App;

class Rover
{
    private string $direction;
    private int $y;
    private int $x;

    public function __construct(int $x, int $y, string $direction)
    {
        $this->direction = $direction;
        $this->y = $y;
        $this->x = $x;
    }

    public function receive(string $commandsSequence): void
    {
        $commandsSequenceLenght = strlen($commandsSequence);
        for ($i = 0; $i < $commandsSequenceLenght; ++$i) {
            $command = substr($commandsSequence, $i, 1);
            if ($command === "l" || $command === "r") {
                // Rotate Rover
                switch ($this->direction) { 
                    case 'N': { 
                        switch($command) { 
                            case 'r': { 
                                $this->direction = "E"; 
                            break; 
                            } 

                            default:
                                $this->direction = "W"; 
                       }  
                    } 
                    case 'S': { 
                        switch($command) { 
                            case 'r': { 
                                $this->direction = "W"; 
                            break; 
                            } 

                            default:
                                $this->direction = "E"; 
                       }  
                    } 
                    case 'W': { 
                        switch($command) { 
                            case 'r': { 
                                $this->direction = "N"; 
                            break; 
                            } 

                            default:
                                $this->direction = "S"; 
                       }  
                    } 

                    default : 
                        if ($command === "r") {
                            $this->direction = "S";
                        } else {
                            $this->direction = "N";
                        }
                }
        
            } else {
                // Displace Rover
                $displacement1 = -1;

                if ($command === "f") {
                    $displacement1 = 1;
                }
                $displacement = $displacement1;

                switch ($this->direction) { 
                        case 'N' :
                             $this->y += $displacement;
                        break ;
                        case 'S' : 
                            $this->y -= $displacement;
                        break ;
                        case 'W' : 
                            $this->x -= $displacement ;
                        break ;
                        default:
                            $this->x += $displacement;
                }

            }
        }
    }
}
